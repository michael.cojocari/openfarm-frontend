import { Component, inject } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { Plant } from '../models/plant';

@Component({
  selector: 'plant-search',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
  ],
  templateUrl: './plant-search.component.html',
  styleUrl: './plant-search.component.css',
})
export class PlantSearchComponent {
  private fb = inject(FormBuilder);
  plantSearchResults: Plant[] = [
    new Plant('hello', 'affl'),
    new Plant('derp', 'eka'),
  ];
  plantSearchResultsColumnsToDisplay = [
    'symbol',
    'synonymSymbol',
    'scientificNameWithAuthor',
    'stateCommonName',
  ];

  plantSearchForm = this.fb.group({
    searchBox: null,
  });

  public runSearch() {
    console.log('nothing');
  }
}
