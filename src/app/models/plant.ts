export class Plant {
  symbol!: string;
  synonymSymbol!: string;
  scientificNameWithAuthor!: string;
  stateCommonName!: string;

  constructor(
    symbol: string = '',
    synonymSymbol: string = '',
    scientificNameWithAuthor: string = '',
    stateCommonName: string=''
  ) {
    this.symbol = symbol;
    this.synonymSymbol = synonymSymbol;
    this.scientificNameWithAuthor = scientificNameWithAuthor;
    this.stateCommonName = stateCommonName;
  }
}
